import React, { Component } from "react";



class AdminPanel extends Component {
  render() {
    return (
      <div
        className="row"
        style={{
          padding: ".2rem",
          margin: ".5rem",
          width: "inherit",
          border: ".5rem double #1a1a1a",
          backgroundColor: "#f7f7f7",
          height: "90vh"
        }}
      >
        <div className="col m3" style={{ height: "100%", overflowY: "auto" }}>
         
        </div>
        <div className="col m5" style={{ height: "100%", overflowY: "auto" }}>
          
        </div>
        <div className="col m4" style={{ height: "100%", overflowY: "auto" }}>
         
        </div>
      </div>
    );
  }
}

export default AdminPanel;
