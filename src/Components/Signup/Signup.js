import React, { Component } from 'react';
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import axios from 'axios';
import "./Signup.css";

export default class Registration extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      password_confirmation: "",
      registrationErrors: ""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  
  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    const { name, surname, email, password, password_confirmation } = this.state;

    axios
      .post(
        "http://localhost:3001/registrations",
        {
          user: {
            name: name,
            surname: surname,
            email: email,
            password: password,
            password_confirmation: password_confirmation
          }
        },
        { withCredentials: true }
      )
      .then(response => {
        if (response.data.status === "created") {
          this.props.handleSuccessfulAuth(response.data);
        }
      })
      .catch(error => {
        console.log("registration error", error);
      });
    event.preventDefault();
  }
  

  render() {
    return (
      <div className="Login">
        <Form onSubmit={this.handleSubmit}>

        <Form.Group size="lg" controlId="Name">
          <Form.Label>Name</Form.Label>
          <Form.Control
            autoFocus
            type="Name"
            name="Name"
            placeholder="Name"
            value={this.state.name}
            onChange={this.handleChange}
            required
          />
        </Form.Group>

        <Form.Group size="lg" controlId="Surname">
          <Form.Label>Surname</Form.Label>
          <Form.Control
            type="Surname"
            name="Surname"
            placeholder="Surname"
            value={this.state.Surname}
            onChange={this.handleChange}
            required
          />
        </Form.Group>


        <Form.Group size="lg" controlId="email">
          <Form.Label>email</Form.Label>
          <Form.Control
            type="email"
            name="email"
            placeholder="email"
            value={this.state.email}
            onChange={this.handleChange}
            required
          />
        </Form.Group>

        <Form.Group size="lg" controlId="password">
          <Form.Label>password</Form.Label>
          <Form.Control
            type="password"
            name="password"
            placeholder="password"
            value={this.state.password}
            onChange={this.handleChange}
            required
          />
        </Form.Group>

        <Form.Group size="lg" controlId="password">
          <Form.Label>password</Form.Label>
          <Form.Control
            type="password"
            name="password_confirmation"
            placeholder="password confirmation"
            value={this.state.password_confirmation}
            onChange={this.handleChange}
            required
          />
        </Form.Group>
      
          <Button block size="lg" type="submit" disabled={this.handleSubmit}>
            SignUp
          </Button>
        </Form>
      </div>
    );
  }
}