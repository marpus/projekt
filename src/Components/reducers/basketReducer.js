import { ADD_PRODUCT_BASKET, GET_NUMBERS_BASKET } from '../../actions/types'

const initialState = {
    basketNumbers: 0,
    cartCost: 0,
    products: {
        BagelAndCreamCheese: {
            name: 'Bagel And Cream Cheese',
            price: 12,
            numbers: 0,
            inCart: false,
        },
        BreakfastSandwich:{
            name: 'Breakfast Sandwich',
            price: 15,
            numbers: 0,
            inCart: false,

        },
    }
}

export default (state = initialState, action) => {
    switch(action.type){
        case ADD_PRODUCT_BASKET:
            let addQuantity = { ...state.products[action.payload]}

            addQuantity.numbers += 1;
            addQuantity.inCart = true;
            console.log(addQuantity);

            return {
                ...state,
                basketNumbers: state.basketNumbers + 1,
                cartCost: state.cartCost + state.products[action.payload],
                products: {
                    ...state.products,
                    [action.payload]: addQuantity,

                }
            }
            case GET_NUMBERS_BASKET:
                return {
                    ...state
                }
        default:
            return state;
    }
}